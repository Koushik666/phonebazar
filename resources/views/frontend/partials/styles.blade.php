<script src="https://kit.fontawesome.com/acb71761d9.js"></script>

<!-- CSS -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.4/build/css/alertify.min.css"/>

<!-- Bootstrap theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.4/build/css/themes/bootstrap.min.css"/>



<link rel="stylesheet" href="{{ asset('css/app.css') }}" >
<link rel="stylesheet" href="{{ asset('css/style.css') }}?ver=1.1.1">
