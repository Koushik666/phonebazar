@extends('frontend.layouts.master')

@section('content')


    <!-- Start Sidebar + Content -->

    <div class='container margin-top-20'>
        <div class="row">
            <div class="col-md-4">

                @include('frontend.partials.product-sidebar')

            </div>

            <div class="col-md-8">
                <div class="widget">
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="col-md-11 text-center">
                            <h2>You are logged in!</h2>
                        </div>
                    </div>
                </div>
                <div class="widget">

                </div>

            </div>

        </div>
    </div>

    <!-- End Sidebar + Content -->


@endsection



{{--<div class="card-body">--}}
{{--    @if (session('status'))--}}
{{--        <div class="alert alert-success" role="alert">--}}
{{--            {{ session('status') }}--}}
{{--        </div>--}}
{{--    @endif--}}

{{--    You are logged in!--}}
{{--</div>--}}