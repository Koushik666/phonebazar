-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 08, 2019 at 09:18 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_ecommerce_tutorial`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Super Admin' COMMENT 'Admin|Super Admin',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `phone_no`, `avatar`, `type`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Koushik', 'Koushik.saha666@gmail.com', '$2y$10$AcOR6ZeGjcEuaGCzKdNice7H7BmEII4q/dW3Zr3/FCTQjjruHt/vS', '01766777357', NULL, 'Super Admin', 'dW7HFFWWmGxlhP2qlANdQd6oNct1WmJw4ubwkYjVVZMOCMs964Z7PmMveIpS', '2019-08-04 07:49:39', '2019-08-04 12:31:32');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `name`, `description`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Samsung', NULL, '1564856045.jpg', '2019-08-03 12:14:06', '2019-08-03 12:14:06'),
(2, 'Apple', NULL, '1564856065.png', '2019-08-03 12:14:25', '2019-08-03 12:14:25'),
(3, 'Huawei', NULL, '1564856086.png', '2019-08-03 12:14:47', '2019-08-03 12:14:47'),
(4, 'Xiaomi', NULL, '1564856110.png', '2019-08-03 12:15:10', '2019-08-03 12:15:10'),
(5, 'LG', NULL, '1564856126.jpg', '2019-08-03 12:15:26', '2019-08-03 12:15:26'),
(6, 'HTC', NULL, '1564856143.png', '2019-08-03 12:15:43', '2019-08-03 12:15:43'),
(7, 'Sony', NULL, '1564856167.jpg', '2019-08-03 12:16:07', '2019-08-03 12:16:07'),
(8, 'Nokia', NULL, '1564856199.png', '2019-08-03 12:16:39', '2019-08-03 12:16:39'),
(9, 'Motorola', NULL, '1564856229.png', '2019-08-03 12:17:10', '2019-08-03 12:17:10'),
(10, 'Lenovo', NULL, '1564856259.png', '2019-08-03 12:17:40', '2019-08-03 12:17:40'),
(11, 'Google', NULL, '1564856282.jpg', '2019-08-03 12:18:02', '2019-08-03 12:18:02');

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `order_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_quantity` bigint(20) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`id`, `product_id`, `user_id`, `order_id`, `ip_address`, `product_quantity`, `created_at`, `updated_at`) VALUES
(1, 17, NULL, NULL, '::1', 1, '2019-08-08 11:54:55', '2019-08-08 11:54:55');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `image`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, 'Mobile', NULL, '1564856470.png', NULL, '2019-08-03 12:21:10', '2019-08-03 12:21:10'),
(2, 'Laptop', NULL, '1564856489.png', NULL, '2019-08-03 12:21:29', '2019-08-03 12:21:29'),
(3, 'Tablets', NULL, '1564856507.png', NULL, '2019-08-03 12:21:47', '2019-08-03 12:21:47'),
(4, 'DSLR', NULL, '1564856519.png', NULL, '2019-08-03 12:21:59', '2019-08-03 12:21:59'),
(5, 'Camera', NULL, '1564856532.jpg', NULL, '2019-08-03 12:22:12', '2019-08-03 12:22:12'),
(6, 'Desktops', NULL, '1564856548.jpeg', NULL, '2019-08-03 12:22:28', '2019-08-03 12:22:28'),
(7, 'Mobile Phone Accessories', NULL, '1564856565.jpg', NULL, '2019-08-03 12:22:45', '2019-08-03 12:22:45'),
(8, 'Samsung', NULL, '1564856608.jpg', 1, '2019-08-03 12:23:28', '2019-08-03 12:23:28'),
(9, 'Apple', NULL, '1564856634.png', 1, '2019-08-03 12:23:54', '2019-08-03 12:23:54'),
(10, 'Huawei', NULL, '1564856875.png', 1, '2019-08-03 12:27:55', '2019-08-03 12:27:55'),
(11, 'Xiaomi', NULL, '1564856903.png', 1, '2019-08-03 12:28:23', '2019-08-03 12:28:23'),
(12, 'Sony', NULL, '1564856929.jpg', 1, '2019-08-03 12:28:49', '2019-08-03 12:28:49'),
(13, 'Nokia', NULL, '1564856975.png', 1, '2019-08-03 12:29:35', '2019-08-03 12:29:35'),
(14, 'LG', NULL, '1564857201.jpg', 1, '2019-08-03 12:33:21', '2019-08-03 12:33:21'),
(15, 'Motorola', NULL, '1564857224.png', 1, '2019-08-03 12:33:45', '2019-08-03 12:33:45'),
(16, 'HTC', NULL, '1564857259.png', 1, '2019-08-03 12:34:20', '2019-08-03 12:34:20'),
(17, 'Lenevo', NULL, '1564857296.png', 1, '2019-08-03 12:34:57', '2019-08-03 12:34:57'),
(18, 'Watch', NULL, '1565154670.jpg', NULL, '2019-08-06 23:11:10', '2019-08-06 23:11:10'),
(19, 'Apple', NULL, '1565160508.png', 3, '2019-08-07 00:48:28', '2019-08-07 00:48:28');

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE `districts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `division_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`id`, `name`, `division_id`, `created_at`, `updated_at`) VALUES
(1, 'Dhaka', 1, '2019-08-03 13:33:04', '2019-08-03 13:33:04'),
(2, 'Gazipur', 1, '2019-08-03 13:33:55', '2019-08-03 13:33:55'),
(3, 'Kishoreganj', 1, '2019-08-03 13:34:10', '2019-08-03 13:34:10'),
(4, 'Manikganj', 1, '2019-08-03 13:34:33', '2019-08-03 13:34:33'),
(5, 'Munshiganj', 1, '2019-08-03 13:42:52', '2019-08-03 13:42:52'),
(6, 'Narayanganj', 1, '2019-08-03 13:43:07', '2019-08-03 13:43:07'),
(7, 'Narsingdi', 1, '2019-08-03 13:43:19', '2019-08-03 13:43:19'),
(8, 'Tangail', 1, '2019-08-03 13:43:28', '2019-08-03 13:43:28'),
(9, 'Bagerhat', 4, '2019-08-05 01:06:19', '2019-08-05 01:06:19'),
(10, 'Chuadanga', 4, '2019-08-05 01:06:30', '2019-08-05 01:06:30'),
(11, 'Jessore', 4, '2019-08-05 01:06:40', '2019-08-05 01:06:40'),
(12, 'Jhenaidah', 4, '2019-08-05 01:06:50', '2019-08-05 01:06:50'),
(13, 'Khulna', 4, '2019-08-05 01:06:59', '2019-08-05 01:06:59'),
(14, 'Kushtia', 4, '2019-08-05 01:07:10', '2019-08-05 01:07:10'),
(15, 'Magura', 4, '2019-08-05 01:07:20', '2019-08-05 01:07:20'),
(16, 'Meherpur', 4, '2019-08-05 01:07:34', '2019-08-05 01:07:34'),
(17, 'Narail', 4, '2019-08-05 01:07:45', '2019-08-05 01:07:45'),
(18, 'Satkhira', 4, '2019-08-05 01:07:57', '2019-08-05 01:07:57');

-- --------------------------------------------------------

--
-- Table structure for table `divisions`
--

CREATE TABLE `divisions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `priority` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `divisions`
--

INSERT INTO `divisions` (`id`, `name`, `priority`, `created_at`, `updated_at`) VALUES
(1, 'Dhaka', 1, '2019-08-03 13:30:16', '2019-08-03 13:30:16'),
(2, 'Chattagram', 2, '2019-08-03 13:30:34', '2019-08-03 13:30:34'),
(3, 'Barishal', 3, '2019-08-03 13:31:24', '2019-08-03 13:31:24'),
(4, 'Khulna', 4, '2019-08-03 13:31:39', '2019-08-03 13:31:39'),
(5, 'Mymensingh', 5, '2019-08-03 13:32:04', '2019-08-03 13:32:04'),
(6, 'Rajshahi', 6, '2019-08-03 13:32:20', '2019-08-03 13:32:20'),
(7, 'Rangpur', 7, '2019-08-03 13:32:35', '2019-08-03 13:32:35'),
(8, 'Sylhet', 8, '2019-08-03 13:32:49', '2019-08-03 13:32:49');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(22, '2014_10_12_100000_create_password_resets_table', 1),
(38, '2019_07_24_132154_create_admins_table', 9),
(39, '2019_07_24_132027_create_brands_table', 10),
(40, '2019_08_01_141117_create_carts_table', 11),
(41, '2019_07_24_131859_create_categories_table', 12),
(42, '2019_07_30_153850_create_divisions_table', 13),
(43, '2019_07_30_154000_create_districts_table', 14),
(44, '2019_08_01_141003_create_orders_table', 15),
(45, '2019_08_03_084517_create_payments_table', 16),
(46, '2019_07_23_040518_create_products_table', 17),
(47, '2019_07_24_135129_create_product_images_table', 18),
(48, '2019_08_03_075326_create_settings_table', 19),
(49, '2014_10_12_000000_create_users_table', 20),
(50, '2019_08_05_083300_create_sliders_table', 21);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `payment_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `is_paid` tinyint(1) NOT NULL DEFAULT '0',
  `is_completed` tinyint(1) NOT NULL DEFAULT '0',
  `is_seen_by_admin` tinyint(1) NOT NULL DEFAULT '0',
  `transaction_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `shipping_charge` int(11) NOT NULL DEFAULT '60',
  `custom_discount` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `payment_id`, `ip_address`, `name`, `phone_no`, `shipping_address`, `email`, `message`, `is_paid`, `is_completed`, `is_seen_by_admin`, `transaction_id`, `created_at`, `updated_at`, `shipping_charge`, `custom_discount`) VALUES
(4, NULL, 2, '::1', 'Koushik Saha', '01766777357', 'Flat-D6,76-77 North Road, Dhanmondi', 'koushik.saha666@gmail.com', 'Good News', 0, 0, 1, '1391154661', '2019-08-05 23:10:01', '2019-08-06 00:14:48', 80, 20),
(5, NULL, 3, '::1', 'Koushik Saha', '01677662792', 'Flat-D6,76-77 North Road, Dhanmondi', 'Koushiksaha781@gmail.com', 'Order New', 0, 0, 1, '1391154661', '2019-08-05 23:10:52', '2019-08-05 23:11:38', 60, 0);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priority` tinyint(4) NOT NULL DEFAULT '1',
  `short_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Payment no',
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'agent|personal',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `name`, `image`, `priority`, `short_name`, `no`, `type`, `created_at`, `updated_at`) VALUES
(1, 'Cash In Delivery ', 'cash_in.jpg', 1, 'cash_in', NULL, NULL, '2019-08-03 19:52:01', '2019-08-03 19:52:01'),
(2, 'Bkash', 'bkash.jpg', 2, 'bkash', '01766777357', 'personal ', '2019-08-03 19:52:01', '2019-08-03 19:52:01'),
(3, 'Rocket', 'rocket.jpg', 3, 'rocket', '01766777357', 'personal ', '2019-08-03 19:52:01', '2019-08-03 19:52:01');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `brand_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` bigint(20) NOT NULL DEFAULT '1',
  `price` bigint(20) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `offer_price` bigint(20) DEFAULT NULL,
  `admin_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `brand_id`, `title`, `description`, `slug`, `quantity`, `price`, `status`, `offer_price`, `admin_id`, `created_at`, `updated_at`) VALUES
(1, 8, 1, 'Samsung Galaxy Fold', 'Dimensions	Unfolded: 160.9 x 117.9 x 6.9 mm\r\nFolded: 160.9 x 62.9 x 15.5 mm\r\nWeight	263 g (9.28 oz)\r\nSIM	Nano-SIM, Electronic SIM card (eSIM)\r\nNano-SIM - 5G model\r\n 	Samsung Pay (Visa, MasterCard certified)\r\nDISPLAY	Type	Dynamic AMOLED capacitive touchscreen, 16M colors\r\nSize	7.3 inches, 162.6 cm2 (~85.7% screen-to-body ratio)\r\nResolution	1536 x 2152 pixels (~362 ppi density)\r\n 	HDR10+\r\nCover display: 4.6\", Super AMOLED, 720 x 1680 pixels (21:9)', 'samsung-galaxy-fold', 5, 170000, 0, NULL, 1, '2019-08-03 12:47:08', '2019-08-03 12:47:08'),
(2, 8, 1, 'Samsung Galaxy S10 5G', 'Dimensions	162.6 x 77.1 x 7.9 mm (6.40 x 3.04 x 0.31 in)\r\nWeight	198 g (6.98 oz)\r\nBuild	Front/back glass (Gorilla Glass 6), aluminum frame\r\nSIM	Single SIM (Nano-SIM) or Dual SIM (Nano-SIM, dual stand-by)\r\n 	Samsung Pay (Visa, MasterCard certified)\r\nIP68 dust/water proof (up to 1.5m for 30 mins)\r\nDISPLAY	Type	Dynamic AMOLED capacitive touchscreen, 16M colors\r\nSize	6.7 inches, 112.0 cm2 (~89.4% screen-to-body ratio)\r\nResolution	1440 x 3040 pixels, 19:9 ratio (~502 ppi density)\r\nProtection	Corning Gorilla Glass 6\r\n 	HDR10+\r\nAlways-on display', 'samsung-galaxy-s10-5g', 10, 110000, 0, NULL, 1, '2019-08-03 12:50:19', '2019-08-03 12:50:19'),
(3, 8, 1, 'Samsung Galaxy S10+', 'Dimensions	157.6 x 74.1 x 7.8 mm (6.20 x 2.92 x 0.31 in)\r\nWeight	175 g / 198 g (ceramic) (6.17 oz)\r\nBuild	Front glass (Gorilla Glass 6), back glass (Gorilla Glass 5), aluminum frame\r\nFront glass (Gorilla Glass 6), ceramic body\r\nSIM	Single SIM (Nano-SIM) or Hybrid Dual SIM (Nano-SIM, dual stand-by)\r\n 	Samsung Pay (Visa, MasterCard certified)\r\nIP68 dust/water proof (up to 1.5m for 30 mins)\r\nDISPLAY	Type	Dynamic AMOLED capacitive touchscreen, 16M colors\r\nSize	6.4 inches, 103.8 cm2 (~88.9% screen-to-body ratio)\r\nResolution	1440 x 3040 pixels, 19:9 ratio (~522 ppi density)\r\nProtection	Corning Gorilla Glass 6\r\n 	HDR10+\r\nAlways-on display', 'samsung-galaxy-s10', 25, 80000, 0, NULL, 1, '2019-08-03 13:09:24', '2019-08-03 13:09:24'),
(4, 8, 1, 'Samsung Galaxy S10', 'Dimensions	149.9 x 70.4 x 7.8 mm (5.90 x 2.77 x 0.31 in)\r\nWeight	157 g (5.54 oz)\r\nBuild	Back glass (Gorilla Glass 5), aluminum frame\r\nSIM	Single SIM (Nano-SIM) or Hybrid Dual SIM (Nano-SIM, dual stand-by)\r\n 	Samsung Pay (Visa, MasterCard certified)\r\nIP68 dust/water proof (up to 1.5m for 30 mins)\r\nDISPLAY	Type	Dynamic AMOLED capacitive touchscreen, 16M colors\r\nSize	6.1 inches, 93.2 cm2 (~88.3% screen-to-body ratio)\r\nResolution	1440 x 3040 pixels, 19:9 ratio (~550 ppi density)\r\nProtection	Corning Gorilla Glass 6\r\n 	HDR10+\r\nAlways-on display', 'samsung-galaxy-s10', 100, 62000, 0, NULL, 1, '2019-08-03 13:12:17', '2019-08-03 13:12:17'),
(5, 8, 1, 'Samsung Galaxy S10e', 'Dimensions	142.2 x 69.9 x 7.9 mm (5.60 x 2.75 x 0.31 in)\r\nWeight	150 g (5.29 oz)\r\nBuild	Front/back glass (Gorilla Glass 5), aluminum frame\r\nSIM	Single SIM (Nano-SIM) or Hybrid Dual SIM (Nano-SIM, dual stand-by)\r\n 	Samsung Pay (Visa, MasterCard certified)\r\nIP68 dust/water proof (up to 1.5m for 30 mins)\r\nDISPLAY	Type	Dynamic AMOLED capacitive touchscreen, 16M colors\r\nSize	5.8 inches, 82.8 cm2 (~83.3% screen-to-body ratio)\r\nResolution	1080 x 2280 pixels, 19:9 ratio (~438 ppi density)\r\nProtection	Corning Gorilla Glass 5\r\n 	HDR10+\r\nAlways-on display', 'samsung-galaxy-s10e', 65, 55000, 0, NULL, 1, '2019-08-03 13:15:48', '2019-08-03 13:15:48'),
(6, 8, 1, 'Samsung Galaxy M40', 'Dimensions	155.3 x 73.9 x 7.9 mm (6.11 x 2.91 x 0.31 in)\r\nWeight	168 g (5.93 oz)\r\nBuild	Front glass, plastic body\r\nSIM	Hybrid Dual SIM (Nano-SIM, dual stand-by)\r\nDISPLAY	Type	PLS TFT capacitive touchscreen, 16M colors\r\nSize	6.3 inches, 97.4 cm2 (~84.9% screen-to-body ratio)\r\nResolution	1080 x 2340 pixels, 19.5:9 ratio (~409 ppi density)\r\nProtection	Corning Gorilla Glass (unspecified)', 'samsung-galaxy-m40', 125, 20000, 0, NULL, 1, '2019-08-03 13:18:54', '2019-08-03 13:18:54'),
(7, 8, 1, 'Samsung Galaxy M30', 'Dimensions	159 x 75.1 x 8.5 mm (6.26 x 2.96 x 0.33 in)\r\nWeight	174 g (6.14 oz)\r\nBuild	Front glass, plastic body\r\nSIM	Dual SIM (Nano-SIM, dual stand-by)\r\nDISPLAY	Type	Super AMOLED capacitive touchscreen, 16M colors\r\nSize	6.4 inches, 100.5 cm2 (~84.2% screen-to-body ratio)\r\nResolution	1080 x 2340 pixels, 19.5:9 ratio (~403 ppi density)', 'samsung-galaxy-m30', 30, 26000, 0, NULL, 1, '2019-08-03 13:21:27', '2019-08-03 13:21:27'),
(8, 8, 1, 'Samsung Galaxy M20', 'Dimensions	156.4 x 74.5 x 8.8 mm (6.16 x 2.93 x 0.35 in)\r\nWeight	186 g (6.56 oz)\r\nBuild	Front glass, plastic body\r\nSIM	Dual SIM (Nano-SIM, dual stand-by)\r\nDISPLAY	Type	PLS TFT capacitive touchscreen, 16M colors\r\nSize	6.3 inches, 97.4 cm2 (~83.6% screen-to-body ratio)\r\nResolution	1080 x 2340 pixels, 19.5:9 ratio (~409 ppi density)', 'samsung-galaxy-m20', 15, 25000, 0, NULL, 1, '2019-08-03 13:23:49', '2019-08-03 13:23:49'),
(9, 8, 1, 'Samsung Galaxy M10', 'Dimensions	155.6 x 75.6 x 7.7 mm (6.13 x 2.98 x 0.30 in)\r\nWeight	163 g (5.75 oz)\r\nBuild	Front glass, plastic body\r\nSIM	Dual SIM (Nano-SIM, dual stand-by)\r\nDISPLAY	Type	PLS TFT capacitive touchscreen, 16M colors\r\nSize	6.22 inches, 96.6 cm2 (~82.1% screen-to-body ratio)\r\nResolution	720 x 1520 pixels, 19:9 ratio (~270 ppi density)', 'samsung-galaxy-m10', 200, 14000, 0, NULL, 1, '2019-08-03 13:25:51', '2019-08-03 13:25:51'),
(10, 8, 1, 'Samsung Galaxy A80', 'Dimensions	165.2 x 76.5 x 9.3 mm (6.50 x 3.01 x 0.37 in)\r\nWeight	220 g (7.76 oz)\r\nBuild	Front glass (Gorilla Glass 3), back glass (Gorilla Glass 6), aluminum frame\r\nSIM	Single SIM (Nano-SIM) or Dual SIM (Nano-SIM, dual stand-by)\r\nDISPLAY	Type	Super AMOLED capacitive touchscreen, 16M colors\r\nSize	6.7 inches, 108.4 cm2 (~85.8% screen-to-body ratio)\r\nResolution	1080 x 2400 pixels, 20:9 ratio (~393 ppi density)\r\nProtection	Corning Gorilla Glass 3\r\n 	Always-on display', 'samsung-galaxy-a80', 10, 65000, 0, NULL, 1, '2019-08-03 13:28:48', '2019-08-06 21:18:15'),
(15, 18, 2, 'Apple iPad Air (2019)', 'Dimensions	250.6 x 174.1 x 6.1 mm (9.87 x 6.85 x 0.24 in)\r\nWeight	456 g (Wi-Fi) / 464 g (3G/LTE) (1.01 lb)\r\nSIM	Nano-SIM, Electronic SIM card (eSIM)\r\n 	Stylus support\r\nDISPLAY	Type	IPS LCD capacitive touchscreen, 16M colors\r\nSize	10.5 inches, 341.4 cm2 (~78.3% screen-to-body ratio)\r\nResolution	1668 x 2224 pixels, 4:3 ratio (~265 ppi density)\r\nProtection	Scratch-resistant glass, oleophobic coating\r\n 	DCI-P3\r\nTrue-tone', 'apple-ipad-air-2019', 10, 52000, 0, NULL, 1, '2019-08-07 00:40:54', '2019-08-07 00:40:54'),
(16, 19, 2, 'Apple iPad mini (2019)', 'BODY	Dimensions	203.2 x 134.8 x 6.1 mm (8.0 x 5.31 x 0.24 in)\r\nWeight	300.5 g (Wi-Fi) / 308.2 g (3G/LTE) (10.86 oz)\r\nSIM	Nano-SIM, Electronic SIM card (eSIM)\r\n 	Stylus support\r\nDISPLAY	Type	IPS LCD capacitive touchscreen, 16M colors\r\nSize	7.9 inches, 193.3 cm2 (~70.6% screen-to-body ratio)\r\nResolution	1536 x 2048 pixels, 4:3 ratio (~324 ppi density)\r\nProtection	Scratch-resistant glass, oleophobic coating\r\n 	DCI-P3\r\nTrue-tone', 'apple-ipad-mini-2019', 10, 43000, 0, NULL, 1, '2019-08-07 00:54:23', '2019-08-07 00:54:23'),
(17, 9, 2, 'Apple iPhone XS Max', 'BODY	Dimensions	157.5 x 77.4 x 7.7 mm (6.20 x 3.05 x 0.30 in)\r\nWeight	208 g (7.34 oz)\r\nBuild	Front/back glass, stainless steel frame\r\nSIM	Single SIM (Nano-SIM) or Dual SIM (Nano-SIM, dual stand-by) - for China\r\n 	IP68 dust/water resistant (up to 2m for 30 mins)\r\nApple Pay (Visa, MasterCard, AMEX certified)\r\nDISPLAY	Type	OLED capacitive touchscreen, 16M colors\r\nSize	6.5 inches, 102.9 cm2 (~84.4% screen-to-body ratio)\r\nResolution	1242 x 2688 pixels, 19.5:9 ratio (~458 ppi density)\r\nProtection	Scratch-resistant glass, oleophobic coating\r\n 	Dolby Vision; HDR10; Wide color gamut; 3D Touch; True-tone; 120 Hz touch-sensing', 'apple-iphone-xs-max', 5, 130000, 0, NULL, 1, '2019-08-07 01:18:49', '2019-08-07 01:18:49'),
(18, 9, 2, 'Apple iPhone XS', 'BODY	Dimensions	143.6 x 70.9 x 7.7 mm (5.65 x 2.79 x 0.30 in)\r\nWeight	177 g (6.24 oz)\r\nBuild	Front/back glass, stainless steel frame\r\nSIM	Nano-SIM, Electronic SIM card (eSIM)\r\n 	IP68 dust/water resistant (up to 2m for 30 mins)\r\nApple Pay (Visa, MasterCard, AMEX certified)\r\nDISPLAY	Type	OLED capacitive touchscreen, 16M colors\r\nSize	5.8 inches, 84.4 cm2 (~82.9% screen-to-body ratio)\r\nResolution	1125 x 2436 pixels, 19.5:9 ratio (~458 ppi density)\r\nProtection	Scratch-resistant glass, oleophobic coating\r\n 	Dolby Vision; HDR10; Wide color gamut; 3D Touch; True-tone; 120 Hz touch-sensing', 'apple-iphone-xs', 16, 120000, 0, NULL, 1, '2019-08-07 09:35:20', '2019-08-07 09:35:20');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `product_id`, `image`, `created_at`, `updated_at`) VALUES
(1, 1, '1564858028.jpg', '2019-08-03 12:47:08', '2019-08-03 12:47:08'),
(2, 1, '1564858029.jpg', '2019-08-03 12:47:09', '2019-08-03 12:47:09'),
(3, 1, '1564858029.jpg', '2019-08-03 12:47:09', '2019-08-03 12:47:09'),
(4, 1, '1564858029.jpg', '2019-08-03 12:47:09', '2019-08-03 12:47:09'),
(5, 1, '1564858029.jpg', '2019-08-03 12:47:09', '2019-08-03 12:47:09'),
(6, 2, '1564858219.jpg', '2019-08-03 12:50:19', '2019-08-03 12:50:19'),
(7, 2, '1564858219.jpg', '2019-08-03 12:50:19', '2019-08-03 12:50:19'),
(8, 2, '1564858219.jpg', '2019-08-03 12:50:19', '2019-08-03 12:50:19'),
(9, 2, '1564858220.jpg', '2019-08-03 12:50:20', '2019-08-03 12:50:20'),
(10, 3, '1564859364.jpg', '2019-08-03 13:09:25', '2019-08-03 13:09:25'),
(11, 3, '1564859365.jpg', '2019-08-03 13:09:25', '2019-08-03 13:09:25'),
(12, 3, '1564859365.jpg', '2019-08-03 13:09:25', '2019-08-03 13:09:25'),
(13, 3, '1564859365.jpg', '2019-08-03 13:09:25', '2019-08-03 13:09:25'),
(14, 3, '1564859365.jpg', '2019-08-03 13:09:25', '2019-08-03 13:09:25'),
(15, 4, '1564859537.jpg', '2019-08-03 13:12:17', '2019-08-03 13:12:17'),
(16, 4, '1564859537.jpg', '2019-08-03 13:12:17', '2019-08-03 13:12:17'),
(17, 4, '1564859537.jpg', '2019-08-03 13:12:17', '2019-08-03 13:12:17'),
(18, 4, '1564859537.jpg', '2019-08-03 13:12:17', '2019-08-03 13:12:17'),
(19, 5, '1564859748.jpg', '2019-08-03 13:15:48', '2019-08-03 13:15:48'),
(20, 5, '1564859748.jpg', '2019-08-03 13:15:48', '2019-08-03 13:15:48'),
(21, 5, '1564859748.jpg', '2019-08-03 13:15:48', '2019-08-03 13:15:48'),
(22, 5, '1564859748.jpg', '2019-08-03 13:15:48', '2019-08-03 13:15:48'),
(23, 5, '1564859748.jpg', '2019-08-03 13:15:48', '2019-08-03 13:15:48'),
(26, 6, '1564859934.jpg', '2019-08-03 13:18:54', '2019-08-03 13:18:54'),
(27, 6, '1564859934.jpg', '2019-08-03 13:18:54', '2019-08-03 13:18:54'),
(28, 6, '1564859934.jpg', '2019-08-03 13:18:54', '2019-08-03 13:18:54'),
(29, 7, '1564860088.jpg', '2019-08-03 13:21:28', '2019-08-03 13:21:28'),
(30, 7, '1564860088.jpg', '2019-08-03 13:21:28', '2019-08-03 13:21:28'),
(31, 7, '1564860088.jpg', '2019-08-03 13:21:28', '2019-08-03 13:21:28'),
(32, 7, '1564860088.jpg', '2019-08-03 13:21:28', '2019-08-03 13:21:28'),
(33, 7, '1564860088.jpg', '2019-08-03 13:21:28', '2019-08-03 13:21:28'),
(34, 8, '1564860229.jpg', '2019-08-03 13:23:49', '2019-08-03 13:23:49'),
(35, 8, '1564860229.jpg', '2019-08-03 13:23:49', '2019-08-03 13:23:49'),
(36, 8, '1564860229.jpg', '2019-08-03 13:23:50', '2019-08-03 13:23:50'),
(37, 8, '1564860230.jpg', '2019-08-03 13:23:50', '2019-08-03 13:23:50'),
(38, 8, '1564860230.jpg', '2019-08-03 13:23:50', '2019-08-03 13:23:50'),
(39, 9, '1564860352.jpg', '2019-08-03 13:25:52', '2019-08-03 13:25:52'),
(40, 9, '1564860352.jpg', '2019-08-03 13:25:52', '2019-08-03 13:25:52'),
(41, 9, '1564860352.jpg', '2019-08-03 13:25:52', '2019-08-03 13:25:52'),
(42, 9, '1564860352.jpg', '2019-08-03 13:25:52', '2019-08-03 13:25:52'),
(43, 10, '1564860528.jpg', '2019-08-03 13:28:48', '2019-08-03 13:28:48'),
(44, 10, '1564860528.jpg', '2019-08-03 13:28:48', '2019-08-03 13:28:48'),
(45, 10, '1564860528.jpg', '2019-08-03 13:28:48', '2019-08-03 13:28:48'),
(46, 10, '1564860528.jpg', '2019-08-03 13:28:48', '2019-08-03 13:28:48'),
(53, 15, '15651600540.jpg', '2019-08-07 00:40:54', '2019-08-07 00:40:54'),
(54, 15, '15651600551.jpg', '2019-08-07 00:40:55', '2019-08-07 00:40:55'),
(55, 15, '15651600552.jpg', '2019-08-07 00:40:55', '2019-08-07 00:40:55'),
(56, 15, '15651600553.jpg', '2019-08-07 00:40:55', '2019-08-07 00:40:55'),
(57, 16, '15651608630.jpg', '2019-08-07 00:54:23', '2019-08-07 00:54:23'),
(58, 16, '15651608631.jpg', '2019-08-07 00:54:23', '2019-08-07 00:54:23'),
(59, 16, '15651608632.jpg', '2019-08-07 00:54:23', '2019-08-07 00:54:23'),
(60, 16, '15651608633.jpg', '2019-08-07 00:54:23', '2019-08-07 00:54:23'),
(61, 16, '15651608644.jpg', '2019-08-07 00:54:24', '2019-08-07 00:54:24'),
(62, 17, '15651623300.jpg', '2019-08-07 01:18:50', '2019-08-07 01:18:50'),
(63, 17, '15651623301.jpg', '2019-08-07 01:18:50', '2019-08-07 01:18:50'),
(64, 17, '15651623302.jpg', '2019-08-07 01:18:50', '2019-08-07 01:18:50'),
(65, 17, '15651623303.jpg', '2019-08-07 01:18:50', '2019-08-07 01:18:50'),
(66, 17, '15651623304.jpg', '2019-08-07 01:18:50', '2019-08-07 01:18:50'),
(67, 18, '15651921200.jpg', '2019-08-07 09:35:21', '2019-08-07 09:35:21'),
(68, 18, '15651921211.jpg', '2019-08-07 09:35:21', '2019-08-07 09:35:21'),
(69, 18, '15651921212.jpg', '2019-08-07 09:35:21', '2019-08-07 09:35:21');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_cost` bigint(20) UNSIGNED NOT NULL DEFAULT '100',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `email`, `phone`, `address`, `shipping_cost`, `created_at`, `updated_at`) VALUES
(1, 'test@gmail.com', '01766888357', 'Dhaka - 1209, Dhaka', 100, '2019-08-03 20:03:01', '2019-08-03 20:03:01');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `button_text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `button_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priority` tinyint(3) UNSIGNED NOT NULL DEFAULT '10',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `title`, `image`, `button_text`, `button_link`, `priority`, `created_at`, `updated_at`) VALUES
(6, 'Phone Commerce Special Discount', '1565077524.jpg', 'Special DIscount', 'http://localhost/LaraEcommerce/products/samsung-galaxy-m20', 1, '2019-08-06 01:45:24', '2019-08-07 02:53:00'),
(7, '24/7 Online Support', '1565077564.jpg', 'Contact', 'http://localhost/LaraEcommerce/contacts', 2, '2019-08-06 01:46:04', '2019-08-07 02:54:55'),
(8, 'All Samsung Phone L:ist', '1565077625.png', 'Samsung', 'http://localhost/LaraEcommerce/products/category/8', 3, '2019-08-06 01:47:05', '2019-08-07 02:55:43');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `street_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `division_id` bigint(20) UNSIGNED NOT NULL COMMENT 'Division Table ID',
  `district_id` bigint(20) UNSIGNED NOT NULL COMMENT 'District Table ID',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT '0=Inactive|1=Active|2=Ban',
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_address` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `username`, `phone_no`, `email`, `password`, `street_address`, `division_id`, `district_id`, `status`, `ip_address`, `avatar`, `shipping_address`, `remember_token`, `created_at`, `updated_at`) VALUES
(4, 'Koushik', 'Saha', 'koushiksaha', '01766777357', 'koushik.saha666@gmail.com', '$2y$10$Xx1Bb2IUA.6zm.p7DO7qLuRlwzVZN2bpkaBRZ3Ec5s0B7dhEzd8kC', '76 North Road', 1, 3, 1, '::1', NULL, NULL, 'iDjTf7YBiLLjzBUDtz4IozfTf6eSQdpc4l1bK2EhhOq4ppmHxaHGgSkUzZH2', '2019-08-05 22:43:44', '2019-08-05 22:44:05'),
(6, 'Koushik 781', 'Saha', 'koushik-781saha', '01677662792', 'koushiksaha781@gmail.com', '$2y$10$6lUxW7u/QbPc2KMWXkOES.ZnnN1RWzymswdoayoqQklxZzszx5lh6', '76 North Road', 4, 17, 1, '::1', NULL, NULL, 'xBto7zJDh6HflSvJo1snf5nAfDwwCpRpg7fxVLXeHrzO5OFWDH', '2019-08-06 22:43:04', '2019-08-06 22:44:15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `carts_user_id_foreign` (`user_id`),
  ADD KEY `carts_product_id_foreign` (`product_id`),
  ADD KEY `carts_order_id_foreign` (`order_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `districts_division_id_foreign` (`division_id`);

--
-- Indexes for table `divisions`
--
ALTER TABLE `divisions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_user_id_foreign` (`user_id`),
  ADD KEY `orders_payment_id_foreign` (`payment_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `payments_short_name_unique` (`short_name`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_phone_no_unique` (`phone_no`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `districts`
--
ALTER TABLE `districts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `divisions`
--
ALTER TABLE `divisions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `carts`
--
ALTER TABLE `carts`
  ADD CONSTRAINT `carts_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `carts_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `carts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `districts`
--
ALTER TABLE `districts`
  ADD CONSTRAINT `districts_division_id_foreign` FOREIGN KEY (`division_id`) REFERENCES `divisions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_payment_id_foreign` FOREIGN KEY (`payment_id`) REFERENCES `payments` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
