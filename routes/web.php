<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Models\District;

Route::get('/', 'Frontend\PagesController@index')->name('index');
Route::get('/contacts', 'Frontend\PagesController@contacts')->name('contacts');


//
//Product Routes
//All the routes for our product for frontend
Route::group(['prefix' => 'products'], function () {

    Route::get('/', 'Frontend\ProductsController@index')->name('products');
    Route::get('/{slug}', 'Frontend\ProductsController@show')->name('products.show');
    Route::get('/new/search', 'Frontend\PagesController@search')->name('search');

//    Category Routes
    Route::get('/categories', 'Frontend\CategoriesController@index')->name('categories.index');
    Route::get('/category/{id}', 'Frontend\CategoriesController@show')->name('categories.show');

});

//User Routes
Route::group(['prefix' => 'user'], function () {
    Route::get('/token/{token}', 'Frontend\VerificationController@verify')->name('user.verification');
    Route::get('/dashboard', 'Frontend\UsersController@dashboard')->name('user.dashboard');
    Route::get('/profile', 'Frontend\UsersController@profile')->name('user.profile');
    Route::post('/profile/update', 'Frontend\UsersController@profileUpdate')->name('user.profile.update');

});


//Cart Routes
Route::group(['prefix' => 'carts'], function () {
    Route::get('/', 'Frontend\CartsController@index')->name('carts');
    Route::post('/store', 'Frontend\CartsController@store')->name('carts.store');
    Route::post('/update/{id}', 'Frontend\CartsController@update')->name('carts.update');
    Route::post('/delete/{id}', 'Frontend\CartsController@destroy')->name('carts.delete');

});

////Cart Routes
//Route::group(['prefix' => 'carts'], function () {
//    Route::get('/', 'API\CartsController@index')->name('carts');
//    Route::post('/store', 'API\CartsController@store')->name('carts.store');
//    Route::post('/update/{id}', 'API\CartsController@update')->name('carts.update');
//    Route::post('/delete/{id}', 'API\CartsController@destroy')->name('carts.delete');
//
//});


//checkout Routes
Route::group(['prefix' => 'checkout'], function () {
    Route::get('/', 'Frontend\CheckoutsController@index')->name('checkouts');
    Route::post('/store', 'Frontend\CheckoutsController@store')->name('checkouts.store');

});

//Admin Routes
Route::group(['prefix' => 'admin'], function () {
    Route::get('/', 'Backend\PagesController@index')->name('admin.index');

    //Admin Login Routes
    Route::get('/login', 'Auth\Admin\LoginController@showLoginForm')->name('admin.login');
    Route::post('/login/submit', 'Auth\Admin\LoginController@login')->name('admin.login.submit');
    Route::post('/logout/submit', 'Auth\Admin\LoginController@logout')->name('admin.logout');


    //Password Email Send
    Route::get('/password/reset', 'Auth\Admin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::post('/password/resetPost', 'Auth\Admin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');

    //Password Reset
    Route::get('/password/reset/{token}', 'Auth\Admin\ResetPasswordController@showResetForm')->name('admin.password.reset');
    Route::post('/password/reset', 'Auth\Admin\ResetPasswordController@reset')->name('admin.password.reset.post');


//  Product Route
    Route::group(['prefix' => '/products'], function () {

        Route::get('/', 'Backend\ProductsController@index')->name('admin.products');
        Route::get('/create', 'Backend\ProductsController@create')->name('admin.product.create');
        Route::get('/{id}', 'Backend\ProductsController@edit')->name('admin.product.edit');

        \
        Route::post('/store', 'Backend\ProductsController@store')->name('admin.product.store');

        Route::post('/product/edit/{id}', 'Backend\ProductsController@update')->name('admin.product.update');
        Route::post('/product/delete/{id}', 'Backend\ProductsController@delete')->name('admin.product.delete');
    });


    //  Orders Route
    Route::group(['prefix' => '/orders'], function () {

        Route::get('/', 'Backend\OrdersController@index')->name('admin.orders');
        Route::get('/view/{id}', 'Backend\OrdersController@show')->name('admin.order.show');
        Route::post('/delete/{id}', 'Backend\OrdersController@delete')->name('admin.order.delete');

        Route::post('/completed/{id}', 'Backend\OrdersController@completed')->name('admin.order.completed');
        Route::post('/paid/{id}', 'Backend\OrdersController@paid')->name('admin.order.paid');
        Route::post('/charge-update/{id}', 'Backend\OrdersController@chargeUpdate')->name('admin.order.charge');

        Route::get('/invoice/{id}', 'Backend\OrdersController@generateInvoice')->name('admin.order.invoice');

    });


    //  Category Route
    Route::group(['prefix' => '/categories'], function () {

        Route::get('/', 'Backend\CategoriesController@index')->name('admin.categories');
        Route::get('/create', 'Backend\CategoriesController@create')->name('admin.category.create');
        Route::get('/{id}', 'Backend\CategoriesController@edit')->name('admin.category.edit');


        Route::post('/store', 'Backend\CategoriesController@store')->name('admin.category.store');

        Route::post('/category/edit/{id}', 'Backend\CategoriesController@update')->name('admin.category.update');
        Route::post('/category/delete/{id}', 'Backend\CategoriesController@delete')->name('admin.category.delete');
    });

    //  Brand Route
    Route::group(['prefix' => '/brands'], function () {

        Route::get('/', 'Backend\BrandsController@index')->name('admin.brands');
        Route::get('/create', 'Backend\BrandsController@create')->name('admin.brand.create');
        Route::get('/{id}', 'Backend\BrandsController@edit')->name('admin.brand.edit');


        Route::post('/store', 'Backend\BrandsController@store')->name('admin.brand.store');

        Route::post('/brand/edit/{id}', 'Backend\BrandsController@update')->name('admin.brand.update');
        Route::post('/brand/delete/{id}', 'Backend\BrandsController@delete')->name('admin.brand.delete');
    });

    //Division Route
    Route::group(['prefix' => '/divisions'],function()
    {
        Route::get('/','Backend\DivisionsController@index')->name('admin.divisions');
        Route::get('/create','Backend\DivisionsController@create')->name('admin.divisions.create');
        Route::get('/edit/{id}','Backend\DivisionsController@edit')->name('admin.divisions.edit');

        Route::post('/create','Backend\DivisionsController@store')->name('admin.divisions.store');
        Route::post('/divisions/edit/{id}','Backend\DivisionsController@update')->name('admin.divisions.update');
        Route::post('/divisions/delete/{id}','Backend\DivisionsController@delete')->name('admin.divisions.delete');

    });

    //District Route
    Route::group(['prefix' => '/districts'],function()
    {
        Route::get('/','Backend\DistrictsController@index')->name('admin.districts');
        Route::get('/create','Backend\DistrictsController@create')->name('admin.districts.create');
        Route::get('/edit/{id}','Backend\DistrictsController@edit')->name('admin.districts.edit');

        Route::post('/create','Backend\DistrictsController@store')->name('admin.districts.store');
        Route::post('/districts/edit/{id}','Backend\DistrictsController@update')->name('admin.districts.update');
        Route::post('/districts/delete/{id}','Backend\DistrictsController@delete')->name('admin.districts.delete');

    });


    //Slider Route
    Route::group(['prefix' => '/sliders'],function()
    {
        Route::get('/','Backend\SlidersController@index')->name('admin.sliders');
        Route::post('/store','Backend\SlidersController@store')->name('admin.slider.store');
        Route::post('/slider/edit/{id}','Backend\SlidersController@update')->name('admin.slider.update');
        Route::post('/slider/delete/{id}','Backend\SlidersController@delete')->name('admin.slider.delete');

    });
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//API routes
Route::get('get-districts/{id}', function($id){
    return json_encode(App\Models\District::where('division_id',$id)->get());
});